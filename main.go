package main

import (
	"net/http"

	"gitlab.com/marceloeduardo244/go-web/routes"
)

func main() {
	routes.CarregaRotas()
	http.ListenAndServe(
		":8000", nil,
	)
}
