# Alura Loja
- Projeto mvc, com curd completo.
- Paginas html embutidas na  api.

# Banco de dados
- Conexão com banco postgres.
- Acesso ao banco via pgadmin.
- Postgres e pgadmin estão no arquivo docker-compose.

# Executando a aplicação
- go run main.go ou go build (na raiz do projeto)
