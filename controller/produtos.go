package controller

import (
	"net/http"
	"strconv"
	"text/template"

	"gitlab.com/marceloeduardo244/go-web/models"
)

var temp = template.Must(template.ParseGlob("templates/*.html"))

func PageIndex(w http.ResponseWriter, r *http.Request) {
	todosOsProdutos := models.BuscaTodosOsProdutos()

	temp.ExecuteTemplate(w, "Index", todosOsProdutos)
}

func PageNewProduct(w http.ResponseWriter, r *http.Request) {
	temp.ExecuteTemplate(w, "AddNewProduct", nil)
}

func PageEditProduct(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	produto := models.BuscaProdutoPorId(id)

	temp.ExecuteTemplate(w, "EditProduct", produto)
}

func InsertNewProduct(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		nome := r.FormValue("nome")
		descricao := r.FormValue("descricao")
		preco := r.FormValue("preco")
		quantidade := r.FormValue("quantidade")

		precoConvertido, err := strconv.ParseFloat(preco, 64)

		if err != nil {
			panic("Erro ao converter preço")
		}

		quantidadeConvertido, err2 := strconv.Atoi(quantidade)

		if err2 != nil {
			panic("Erro ao converter quantidade")
		}

		models.SalvaNovoProduto(nome, descricao, precoConvertido, quantidadeConvertido)
	}
	http.Redirect(w, r, "/", http.StatusMovedPermanently)
}

func DeleteProduct(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")

	models.DeletarProduto(id)

	http.Redirect(w, r, "/", http.StatusMovedPermanently)
}

func UpdateProduct(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		id := r.URL.Query().Get("id")

		nome := r.FormValue("nome")
		descricao := r.FormValue("descricao")
		preco := r.FormValue("preco")
		quantidade := r.FormValue("quantidade")

		precoConvertido, err := strconv.ParseFloat(preco, 64)

		if err != nil {
			panic("Erro ao converter preço")
		}

		quantidadeConvertido, err2 := strconv.Atoi(quantidade)

		if err2 != nil {
			panic("Erro ao converter quantidade")
		}

		models.EditarProduto(id, nome, descricao, precoConvertido, quantidadeConvertido)
	}

	http.Redirect(w, r, "/", http.StatusMovedPermanently)
}
