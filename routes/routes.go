package routes

import (
	"net/http"

	"gitlab.com/marceloeduardo244/go-web/controller"
)

func CarregaRotas() {
	http.HandleFunc("/", controller.PageIndex)
	http.HandleFunc("/new", controller.PageNewProduct)
	http.HandleFunc("/insert", controller.InsertNewProduct)
	http.HandleFunc("/delete", controller.DeleteProduct)
	http.HandleFunc("/edit", controller.PageEditProduct)
	http.HandleFunc("/update", controller.UpdateProduct)
}
