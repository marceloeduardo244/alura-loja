package models

import "gitlab.com/marceloeduardo244/go-web/db"

type Produto struct {
	Id         int
	Nome       string
	Descricao  string
	Preco      float64
	Quantidade int
}

func BuscaTodosOsProdutos() []Produto {
	conexaoComDb := db.ConectaComBd()

	selectDeTodosOsProdutos, err := conexaoComDb.Query("select * from produtos")

	if err != nil {
		panic(err.Error())
	}

	p := Produto{}
	produtos := []Produto{}

	for selectDeTodosOsProdutos.Next() {
		var id, quantidade int
		var nome, descricao string
		var preco float64

		err = selectDeTodosOsProdutos.Scan(&id, &nome, &descricao, &preco, &quantidade)

		if err != nil {
			panic(err.Error())
		}

		p.Id = id
		p.Nome = nome
		p.Descricao = descricao
		p.Preco = preco
		p.Quantidade = quantidade

		produtos = append(produtos, p)
	}

	defer conexaoComDb.Close()
	return produtos
}

func BuscaProdutoPorId(idBusca string) Produto {
	conexaoComDb := db.ConectaComBd()

	selectDeTodosOsProdutos, err := conexaoComDb.Query("select * from produtos where id =" + idBusca)
	if err != nil {
		panic(err.Error())
	}

	p := Produto{}

	var id, quantidade int
	var nome, descricao string
	var preco float64
	selectDeTodosOsProdutos.Next()
	err = selectDeTodosOsProdutos.Scan(&id, &nome, &descricao, &preco, &quantidade)

	if err != nil {
		panic(err.Error())
	}

	p.Id = id
	p.Nome = nome
	p.Descricao = descricao
	p.Preco = preco
	p.Quantidade = quantidade

	defer conexaoComDb.Close()
	return p
}

func SalvaNovoProduto(nome, descricao string, preco float64, quantidade int) {
	conexaoComDb := db.ConectaComBd()

	insereDadosNoBanco, err := conexaoComDb.Prepare("insert into produtos(nome, descricao, preco, quantidade) values ($1, $2, $3, $4)")
	insereDadosNoBanco.Exec(nome, descricao, preco, quantidade)
	if err != nil {
		panic(err.Error())
	}

	defer conexaoComDb.Close()
}

func DeletarProduto(id string) {
	conexaoComDb := db.ConectaComBd()

	insereDadosNoBanco, err := conexaoComDb.Prepare("delete from produtos where id = $1")
	insereDadosNoBanco.Exec(id)
	if err != nil {
		panic(err.Error())
	}

	defer conexaoComDb.Close()
}

func EditarProduto(id string, nome, descricao string, preco float64, quantidade int) {
	conexaoComDb := db.ConectaComBd()

	insereDadosNoBanco, err := conexaoComDb.Prepare("update produtos set nome = $1, descricao = $2, preco = $3, quantidade = $4 where id = $5")
	insereDadosNoBanco.Exec(nome, descricao, preco, quantidade, id)
	if err != nil {
		panic(err.Error())
	}

	defer conexaoComDb.Close()
}
